FROM gitpod/workspace-python-3.11:latest

##### python3 is builtin and zllibc is outdated
RUN sudo install-packages gcc g++ make bison libreadline-dev gawk libtool pkg-config gnulib libcap-dev rsync busybox curl wget squashfs-tools flex perl zip unzip tcl bzip2 locales git xsltproc libncurses-dev gettext sudo bc subversion fakeroot