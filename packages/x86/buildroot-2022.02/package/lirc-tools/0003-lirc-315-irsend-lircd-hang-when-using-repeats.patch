From b91155c2aa5c49de53f9e686f8cc44a377edb991 Mon Sep 17 00:00:00 2001
From: A Sun <asunxx@users.sourceforge.net>
Date: Tue, 3 Apr 2018 18:24:06 -0400
Subject: [PATCH] lirc: #315 irsend / lircd hang when using repeats

irsend SEND_ONCE IR may stall forever for lircd.conf remote control buttons
with min_repeat setting 2 or higher. irsend SEND_START IR may stall forever
for any remote control buttons.

The root causes of IR send stall are due to race timing of SIGALRM in
lircd, used for IR repeat timing, with the time necessary for lircd code
execution to reach its main poll system call.

Patch replaces lircd IR repeat sigalrm timing method with poll timeout and
time of day clock comparison. Poll timeout method is already in use for
lircd IR release_time functionality. And is immune from the SIGALRM
method's timing race.

Correct possible premature timeout for mywaitfordata(maxusecs) where
maxusecs > 0. Premature timeout may occur when release_time
(and this patch's repeat_time) delay remaining is less than maxusecs
delay remaining.

Signed-off-by: A Sun <asunxx@users.sourceforge.net>
---
 daemons/lircd.cpp | 100 +++++++++++++++++++++++++++++++++++-------------------
 1 file changed, 66 insertions(+), 34 deletions(-)

diff --git a/daemons/lircd.cpp b/daemons/lircd.cpp
index d5be708..02731e7 100644
--- a/daemons/lircd.cpp
+++ b/daemons/lircd.cpp
@@ -219,6 +219,7 @@ static struct ir_remote* free_remotes = NULL;
 static int repeat_fd = -1;
 static char* repeat_message = NULL;
 static uint32_t repeat_max = REPEAT_MAX_DEFAULT;
+static timeval repeat_time = { };
 
 static const char* configfile = NULL;
 static FILE* pidf;
@@ -1157,7 +1158,8 @@ static void schedule_repeat_timer (struct timespec* last)
 	unsigned long secs;
 	lirc_t usecs, gap, diff;
 	struct timespec current;
-	struct itimerval repeat_timer;
+	struct timeval last_tv, gap_tv;
+
 	gap = send_buffer_sum() + repeat_remote->min_remaining_gap;
 	clock_gettime (CLOCK_MONOTONIC, &current);
 	secs = current.tv_sec - last->tv_sec;
@@ -1165,13 +1167,16 @@ static void schedule_repeat_timer (struct timespec* last)
 	usecs = (diff < gap ? gap - diff : 0);
 	if (usecs < 10)
 		usecs = 10;
-	log_trace("alarm in %lu usecs", (unsigned long)usecs);
-	repeat_timer.it_value.tv_sec = 0;
-	repeat_timer.it_value.tv_usec = usecs;
-	repeat_timer.it_interval.tv_sec = 0;
-	repeat_timer.it_interval.tv_usec = 0;
-
-	setitimer(ITIMER_REAL, &repeat_timer, NULL);
+	/* log_trace("alarm in %lu usecs", (unsigned long)usecs); */
+	gap_tv.tv_sec = 0;
+	gap_tv.tv_usec = gap;
+	last_tv.tv_sec = last->tv_sec;
+	last_tv.tv_usec = last->tv_nsec / 1000;
+	log_trace("repeat IR: %d usec, last: %ld.%06ld + %d usec",
+		  gap, last_tv.tv_sec, last_tv.tv_usec, diff);
+
+	timeradd(&last_tv, &gap_tv, &repeat_time);
+	alrm = 1;
 }
 
 void dosigalrm(int sig)
@@ -1562,7 +1567,6 @@ static int send_stop(int fd, char* message, char* arguments)
 {
 	struct ir_remote* remote;
 	struct ir_ncode* code;
-	struct itimerval repeat_timer;
 	int err;
 
 	if (parse_rc(fd, message, arguments, &remote, &code, 0, 0, &err) == 0)
@@ -1584,12 +1588,7 @@ static int send_stop(int fd, char* message, char* arguments)
 			repeat_remote->repeat_countdown = repeat_remote->min_repeat - done;
 			return send_success(fd, message);
 		}
-		repeat_timer.it_value.tv_sec = 0;
-		repeat_timer.it_value.tv_usec = 0;
-		repeat_timer.it_interval.tv_sec = 0;
-		repeat_timer.it_interval.tv_usec = 0;
-
-		setitimer(ITIMER_REAL, &repeat_timer, NULL);
+		timerclear(&repeat_time);
 
 		repeat_remote->toggle_mask_state = 0;
 		repeat_remote = NULL;
@@ -1808,25 +1807,18 @@ void free_old_remotes(void)
 			if (found != NULL) {
 				code = get_code_by_name(found, repeat_code->name);
 				if (code != NULL) {
-					struct itimerval repeat_timer;
-
-					repeat_timer.it_value.tv_sec = 0;
-					repeat_timer.it_value.tv_usec = 0;
-					repeat_timer.it_interval.tv_sec = 0;
-					repeat_timer.it_interval.tv_usec = 0;
-
 					found->last_code = code;
 					found->last_send = repeat_remote->last_send;
 					found->toggle_bit_mask_state = repeat_remote->toggle_bit_mask_state;
 					found->min_remaining_gap = repeat_remote->min_remaining_gap;
 					found->max_remaining_gap = repeat_remote->max_remaining_gap;
 
-					setitimer(ITIMER_REAL, &repeat_timer, &repeat_timer);
+					timerclear(&repeat_time);
 					/* "atomic" (shouldn't be necessary any more) */
 					repeat_remote = found;
 					repeat_code = code;
 					/* end "atomic" */
-					setitimer(ITIMER_REAL, &repeat_timer, NULL);
+					alrm = 0;
 					found = NULL;
 				}
 			} else {
@@ -1863,6 +1855,7 @@ static int mywaitfordata(uint32_t maxusec)
 	int i;
 	int ret, reconnect;
 	struct timeval tv, start, now, timeout, release_time;
+	struct timespec now_ts;
 	loglevel_t oldlevel;
 
 	while (1) {
@@ -1875,9 +1868,15 @@ static int mywaitfordata(uint32_t maxusec)
 				dosighup(SIGHUP);
 				hup = 0;
 			}
-			if (alrm) {
-				dosigalrm(SIGALRM);
-				alrm = 0;
+			if (timerisset(&repeat_time)) {
+				clock_gettime(CLOCK_MONOTONIC, &now_ts);
+				now.tv_sec = now_ts.tv_sec;
+				now.tv_usec = now_ts.tv_nsec / 1000;
+				if (timercmp(&now, &repeat_time, >)) {
+					timerclear(&repeat_time);
+					alrm = 0;
+					dosigalrm(SIGALRM);
+				}
 			}
 			memset(&poll_fds, 0, sizeof(poll_fds));
 			for (i = 0; i < (int)POLLFDS_SIZE; i += 1)
@@ -1928,11 +1927,15 @@ static int mywaitfordata(uint32_t maxusec)
 				}
 				reconnect = 1;
 			}
+			log_trace2("reconnect: %d, tv: %ld.%06ld", reconnect, tv.tv_sec, tv.tv_usec);
 			gettimeofday(&start, NULL);
 			if (maxusec > 0) {
-				tv.tv_sec = maxusec / 1000000;
-				tv.tv_usec = maxusec % 1000000;
+				/* round up to next msec for poll/select */
+				tv.tv_sec = (maxusec + 999) / 1000000;
+				tv.tv_usec = (maxusec + 999) / 1000 % 1000 *
+					     1000;
 			}
+			log_trace2("maxusec: %u, tv: %ld.%06ld", maxusec, tv.tv_sec, tv.tv_usec);
 			if (curr_driver->fd == -1 && use_hw()) {
 				/* try to reconnect */
 				timerclear(&timeout);
@@ -1941,6 +1944,8 @@ static int mywaitfordata(uint32_t maxusec)
 				if (timercmp(&tv, &timeout, >)
 				    || (!reconnect && !timerisset(&tv)))
 					tv = timeout;
+				log_trace2("timeout: %ld.%06ld, tv: %ld.%06ld",
+					   timeout.tv_sec, timeout.tv_usec, tv.tv_sec, tv.tv_usec);
 			}
 			get_release_time(&release_time);
 			if (timerisset(&release_time)) {
@@ -1956,16 +1961,45 @@ static int mywaitfordata(uint32_t maxusec)
 					    || timercmp(&tv, &gap, >))
 						tv = gap;
 				}
+				log_trace2("release_time: %ld.%06ld, now: %ld.%06ld, tv: %ld.%06ld",
+					   release_time.tv_sec, release_time.tv_usec, now.tv_sec, now.tv_usec, tv.tv_sec, tv.tv_usec);
 			}
-			if (timerisset(&tv) || timerisset(&release_time) || reconnect)
+			if (timerisset(&repeat_time)) {
+				clock_gettime(CLOCK_MONOTONIC, &now_ts);
+				now.tv_sec = now_ts.tv_sec;
+				now.tv_usec = now_ts.tv_nsec / 1000;
+				if (timercmp(&now, &repeat_time, >)) {
+					timerclear(&tv);
+				} else {
+					struct timeval gap;
+
+					timersub(&repeat_time, &now, &gap);
+					if (!(timerisset(&tv) || reconnect ||
+					      timerisset(&release_time))
+					    || timercmp(&tv, &gap, >))
+						tv = gap;
+				}
+				log_trace2("repeat_time: %ld.%06ld, now: %ld.%06ld, tv: %ld.%06ld",
+					   repeat_time.tv_sec, repeat_time.tv_usec, now.tv_sec, now.tv_usec, tv.tv_sec, tv.tv_usec);
+			}
+
+
+			if (timerisset(&tv) || timerisset(&release_time) || reconnect ||
+			    timerisset(&repeat_time)) {
 				ret = curl_poll((struct pollfd *) &poll_fds.byindex,
 					         POLLFDS_SIZE,
 					         tv.tv_sec * 1000 + tv.tv_usec / 1000);
-			else
+				log_trace2("poll(%p, %d, %ld) = %d",
+					&poll_fds.byindex, POLLFDS_SIZE,
+					tv.tv_sec*1000 + tv.tv_usec/1000, ret);
+			} else {
 				ret = curl_poll((struct pollfd*)&poll_fds.byindex,
 					         POLLFDS_SIZE,
 						 -1);
-
+				log_trace2("poll(%p, %d, %ld) = %d",
+					&poll_fds.byindex, POLLFDS_SIZE,
+					-1l, ret);
+			}
 			if (ret == -1 && errno != EINTR) {
 				log_perror_err("curl_poll()() failed");
 				raise(SIGTERM);
@@ -1990,8 +2024,6 @@ static int mywaitfordata(uint32_t maxusec)
 			if (free_remotes != NULL)
 				free_old_remotes();
 			if (maxusec > 0) {
-				if (ret == 0)
-					return 0;
 				if (time_elapsed(&start, &now) >= maxusec)
 					return 0;
 				maxusec -= time_elapsed(&start, &now);
-- 
2.11.0

