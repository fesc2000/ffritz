################################################################################
#
# nqptp
#
################################################################################

ALAC_VERSION = 0.0.7
ALAC_SITE = $(call github,mikebrady,alac,$(ALAC_VERSION))

ALAC_LICENSE = MIT, BSD-3-Clause
ALAC_LICENSE_FILES = LICENSES
ALAC_DEPENDENCIES = 
ALAC_INSTALL_STAGING = YES

# git clone, no configure
ALAC_AUTORECONF = YES

ALAC_CONF_OPTS =

ALAC_CONF_ENV += LIBS="$(ALAC_CONF_LIBS)"

$(eval $(autotools-package))
