LIBRESPOT_VERSION = v0.4.2
LIBRESPOT_SITE = https://github.com/librespot-org/librespot.git
LIBRESPOT_SITE_METHOD = git

# Install steps
define LIBRESPOT_INSTALL_TARGET_CMDS
    $(INSTALL) -D \
        $(@D)/target/$(RUSTC_TARGET_NAME)/release/librespot \
        $(TARGET_DIR)/usr/bin/
endef

LIBRESPOT_CARGO_BUILD_OPTS=--no-default-features --features alsa-backend

LIBRESPOT_DEPENDENCIES=host-pkgconf alsa-lib

$(eval $(cargo-package))

