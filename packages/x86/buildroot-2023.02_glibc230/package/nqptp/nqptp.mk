################################################################################
#
# nqptp
#
################################################################################

NQPTP_VERSION = 1.2.4
NQPTP_SITE = $(call github,mikebrady,nqptp,$(NQPTP_VERSION))

NQPTP_LICENSE = MIT, BSD-3-Clause
NQPTP_LICENSE_FILES = LICENSES
NQPTP_DEPENDENCIES = 

# git clone, no configure
NQPTP_AUTORECONF = YES

NQPTP_CONF_OPTS =

NQPTP_CONF_ENV += LIBS="$(NQPTP_CONF_LIBS)"

$(eval $(autotools-package))
