ARM_MAX_FS=19922432

ARM_ROOTIMG = $(PLAT_TMP)/uimage/part_09_ARM_ROOTFS.bin

ifeq ($(OEM_PATCH),y)
ifeq ($(shell test $(FWMAJ) -eq 8; echo $$?),0)
ARM_PATCHES += oem-v3.patch
else
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 63 ; echo $$?),0)
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -le 50 ; echo $$?),0)
ARM_PATCHES += oem-v1.patch
else
ARM_PATCHES += oem-v2.patch
endif
else
ATOM_PATCHES += oem-v3.patch
endif
endif
endif


$(ARM_ROOTIMG): $(PLAT_TMP)/uimage
