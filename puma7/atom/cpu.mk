ATOM_MODFILES = $(shell find $(PLAT_BASE)/atom/mod -type f -o -type d)

ATOM_ROOTIMG = $(PLAT_TMP)/uimage/part_03_ATOM_ROOTFS.bin

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 61 ; echo $$?),0)
ATOM_PATCHES = usb-maxdevcount.patch
else
ATOM_PATCHES = usb-maxdevcount-v1.patch
endif

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
ATOM_PATCHES += profile.patch
else
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 61 ; echo $$?),0)
ATOM_PATCHES += profile-v1.patch
else
ATOM_PATCHES += profile-v2.patch
endif
endif

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 19 ; echo $$?),0)
ATOM_PATCHES += 50-udev-default.patch
ATOM_PATCHES += hotplug-remap-v1.patch
else # >= 7.19
ATOM_PATCHES += 10-console.rules.patch
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
ATOM_PATCHES += 20-rc-net-ffmultid.patch
endif

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 24 ; echo $$?),0)
ATOM_PATCHES += hotplug-remap-v2.patch
else # >= 7.24

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 39 ; echo $$?),0)
ATOM_PATCHES += hotplug-remap-v3.patch
else # >= 7.39
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 61 ; echo $$?),0)
ATOM_PATCHES += hotplug-remap-v4.patch
else # >= 7.61
ATOM_PATCHES += hotplug-remap-v5.patch
endif

endif

endif

endif

ifeq ($(OEM_PATCH),y)
ifeq ($(shell test $(FWMAJ) -eq 8; echo $$?),0)
ATOM_PATCHES += oem-v3.patch
else
ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -lt 63 ; echo $$?),0)

ifeq ($(shell test $(FWMAJ) -eq 7 -a $(FWMIN) -le 50 ; echo $$?),0)
ATOM_PATCHES += oem-v1.patch
else
ATOM_PATCHES += oem-v2.patch
endif

else
ATOM_PATCHES += oem-v3.patch
endif
endif
endif


ATOM_PATCHST=$(ATOM_PATCHES:%=$(ATOM_TMP)/.applied.%)

$(ATOM_ROOTIMG): $(PLAT_TMP)/uimage
